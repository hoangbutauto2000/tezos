# Changelog

## Version next

### EVM Kernel

### EVM Node

### Bug fixes

- Increments the nonce of the transaction even if the transaction fails. (!9534)

### Breaking changes

### Internal
